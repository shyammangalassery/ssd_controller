/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
module name: Main Memory Model
author name: Maximilian Singh
email id: maximilian.singh@student.kit.edu
last update done on 14th May 2014

This module emulates the host main memory with BRAMs.  It provides extra
abstraction by providing functions to access submission and completion queues
and allocate and free PRPs.
*/

`include "global_parameters"

package main_memory_model;

import BRAM::*;
import Vector::*;
import StmtFSM::*;

import global_definitions::*;

typedef enum {
	IDLE,
	START,
	GET_DWORD
} Get_compl_state_type deriving (Bits, Eq);

typedef enum {
	IDLE,
	START
} Nvm_get_data_state_type deriving (Bits, Eq);

typedef enum {
	IDLE,
	START
} Put_cmd_state_type deriving (Bits, Eq);

(* synthesize *)
//(* always_ready *)
//(* always_enabled *)
module mkMain_memory_model(Ifc_main_memory_model);

/*
The main memory emulation which is used by the testbench (host) and the NVMe
Controller.

The RAM has to be initialized to zero, to get the right phase tags.  This should
be automatically the case.  // TODO make sure this is right

TODO TODO BRAM2Port is known to cause problems in synthesis ???
*/
BRAM_Configure lv_cfg_bram_memory = defaultValue;
lv_cfg_bram_memory.loadFormat = tagged Hex ("init_main_memory_model_" + `WDC_STRING + ".txt");
BRAM2Port#(UInt#(TAdd#(TSub#(`MAIN_MEM_ADDR_SIZE, TLog#(`WDC)), TLog#(32))), Bit#(`WDC)) main_mem <- mkBRAM2Server(lv_cfg_bram_memory);

// vector which holds the use status of all PRPs
Vector#(`NO_PRPs, Reg#(Bool)) rg_prp_allocated <- replicateM(mkReg(False));

// holds the address of the requested PRP
Reg#(UInt#(64)) rg_get_prp_addr <- mkReg(0);

// internal dword counter for get prp
Reg#(UInt#(TAdd#(TSub#(TAdd#(`MPS, 12), TLog#(TDiv#(`WDC, 8))), 1))) rg_get_prp_count <-
	mkReg(0);

/* Registers for Get Completion Finitie State Machine */
// state register
Reg#(Get_compl_state_type) rg_get_compl_state <- mkReg(IDLE);
// holds the address of completion queue
Reg#(UInt#(64)) rg_get_compl_cq_addr <- mkReg(0);
// holds the completion data after the FSM finished
Reg#(Vector#(TDiv#(16, TDiv#(`WDC, 8)), Bit#(`WDC))) arr_get_compl_data <- mkRegU();
/*
internal dword counter for get compl
*/
Reg#(UInt#(TLog#(TDiv#(16, TDiv#(`WDC, 8))))) rg_get_compl_dword_count <- mkReg(0);

/* Registers for NVM Get Data Finitie State Machine */
// state register
Reg#(Nvm_get_data_state_type) rg_nvm_get_data_state <- mkReg(IDLE);
// holds the address of the requested data
Reg#(UInt#(64)) rg_nvm_get_data_addr <- mkReg(0);
// holds the length in dwords of the requested data
Reg#(UInt#(32)) rg_nvm_get_data_length <- mkReg(0);
/*
internal dword counter for NVM Get Data
*/
Reg#(UInt#(32)) rg_nvm_get_data_dword_count <- mkReg(0);

/* Wires to generate the request ready signal. */
Wire#(Bool) dwr_out_nvm_get_data_start <- mkDWire(False);
Wire#(bit) dwr_out_nvm_request_ready <- mkDWire(1'b1);

/* Registers for Put Command Finitie State Machine */
// state register
Reg#(Put_cmd_state_type) rg_put_cmd_state <- mkReg(IDLE);
// holds the address of submission queue
Reg#(UInt#(64)) rg_put_cmd_sq_addr <- mkReg(0);
// holds the command to be written
Reg#(Nvm_command_type) rg_put_cmd_command <- mkRegU();
/*
internal dword counter for put cmd
*/
Reg#(UInt#(TLog#(TDiv#(64, TDiv#(`WDC, 8))))) rg_put_cmd_dword_count <- mkReg(0);

   Reg#(UInt#(32)) rg_write_count <- mkReg((`WDC/32));
   Reg#(UInt#(64)) rg_write_address <- mkReg(0);


/* Begin of the Get Completion state machine.

Return a completion.

The address of the completion queue has to be stored in rg_get_compl_cq_addr
before starting the state machine.  The result is saved in
arr_get_compl_data.
*/
rule rl_get_compl_idle_state (rg_get_compl_state == IDLE);
	/* This state is changed indirectly by the rule which calls the fsm. */
	rg_get_compl_dword_count <= 0;
endrule: rl_get_compl_idle_state

rule rl_get_compl_start_state (rg_get_compl_state == START);
	/* Request the data dword wise from main memory. */
	main_mem.portA.request.put(BRAMRequest {
		write: False,
		responseOnWrite: False,
		address:
			unpack(pack(rg_get_compl_cq_addr)[`MAIN_MEM_ADDR_SIZE + valueOf(TLog#(TDiv#(32, 8))) - 1:valueOf(TLog#(TDiv#(`WDC, 8)))]) +
			extend(rg_get_compl_dword_count),
		datain: ?
	});
	rg_get_compl_state <= GET_DWORD;
endrule: rl_get_compl_start_state

rule rl_get_compl_get_dword_state (rg_get_compl_state == GET_DWORD);
	/* Get one dword from main memory and save it. */
	let lv_response <- main_mem.portA.response.get();
	arr_get_compl_data[rg_get_compl_dword_count] <= lv_response;
	if (rg_get_compl_dword_count ==
		fromInteger(valueOf(TSub#(TDiv#(16, TDiv#(`WDC, 8)), 1)))) begin

		rg_get_compl_state <= IDLE;
	end else begin
		rg_get_compl_dword_count <= rg_get_compl_dword_count + 1;
		rg_get_compl_state <= START;
	end
endrule: rl_get_compl_get_dword_state
/* End of the Get Completion state machine. */


/* Begin of the NVM Get Data state machine.

Request data dword wise for NVM.  Response is directly handeled by nvm_get_data_
method.

The address of the requested data has to be stored in rg_nvm_get_data_addr and
the length of the requested data in dwords has to be stored in
rg_nvm_get_data_length before starting the state machine.
*/
rule rl_nvm_get_data_idle_state (rg_nvm_get_data_state == IDLE);
	/* This state is changed indirectly by the rule which calls the fsm. */
	rg_nvm_get_data_dword_count <= 0;
	dwr_out_nvm_request_ready <= 1;
endrule: rl_nvm_get_data_idle_state

rule rl_nvm_get_data_start_state (rg_nvm_get_data_state == START);
	/* Request the data dword wise from main memory. */
	$display("%d: main_memory_model: nvm_get_data: get dword from 0x%x data_length %d counter_data_length %d",
		$stime(), rg_nvm_get_data_addr + fromInteger(valueOf(TDiv#(`WDC, 8))) * extend(rg_nvm_get_data_dword_count), rg_nvm_get_data_length, rg_nvm_get_data_dword_count);
   $display ("%d: main_memory_model : address sent to bram: 0x%x",$stime(), unpack(pack(rg_nvm_get_data_addr)[`MAIN_MEM_ADDR_SIZE + valueOf(TLog#(TDiv#(32, 8))) - 1:valueOf(TLog#(TDiv#(`WDC, 8)))]) +
	     rg_nvm_get_data_dword_count);
	main_mem.portB.request.put(BRAMRequest {
		write: False,
		responseOnWrite: False,
		address:
			unpack(pack(rg_nvm_get_data_addr)[`MAIN_MEM_ADDR_SIZE + valueOf(TLog#(TDiv#(32, 8))) - 1:valueOf(TLog#(TDiv#(`WDC, 8)))]) +
			truncate(rg_nvm_get_data_dword_count),
		datain: ?
	});
	if (rg_nvm_get_data_dword_count == rg_nvm_get_data_length - 1)
		rg_nvm_get_data_state <= IDLE;
	else
		rg_nvm_get_data_dword_count <= rg_nvm_get_data_dword_count + 1;
endrule: rl_nvm_get_data_start_state
/* End of the NVM Get Data state machine. */


/* Begin of Put Command state machine.

Write a command into a submission queue.

Before starting the FSM the submission queue address has to be saved in
rg_put_cmd_sq_addr and the command in rg_put_cmd_command.  This state
machine is used by the _put_cmd and the _put_cmd_wait methods.
*/
rule rl_put_cmd_idle_state (rg_put_cmd_state == IDLE);
	/* This state is changed indirectly by the rule which calls the fsm. */
	rg_put_cmd_dword_count <= 0;
endrule: rl_put_cmd_idle_state

rule rl_put_cmd_start_state (rg_put_cmd_state == START  && rg_get_compl_state == IDLE);
	/* Write one dword of the command to the main memory. */
	Bit#(TLog#(SizeOf#(Nvm_command_type))) lv_high_index =
		(extend(pack(rg_put_cmd_dword_count)) + 1) * `WDC - 1;
	Bit#(TLog#(SizeOf#(Nvm_command_type))) lv_low_index =
		extend(pack(rg_put_cmd_dword_count)) * `WDC;
	main_mem.portA.request.put(BRAMRequest {
		write: True,
		responseOnWrite: False,
		// drop 2 LSBs because of DWord addressing
		address:
			unpack(pack(rg_put_cmd_sq_addr)[`MAIN_MEM_ADDR_SIZE + valueOf(TLog#(TDiv#(32, 8))) - 1:valueOf(TLog#(TDiv#(`WDC, 8)))]) +
			extend(rg_put_cmd_dword_count),
		datain:
			pack(rg_put_cmd_command)[lv_high_index:lv_low_index]
	});

	if (rg_put_cmd_dword_count == fromInteger(valueOf(TSub#(TDiv#(64, TDiv#(`WDC, 8)), 1))))
		rg_put_cmd_state <= IDLE;
	else
		rg_put_cmd_dword_count <= rg_put_cmd_dword_count + 1;
endrule: rl_put_cmd_start_state
/* End of put command state machine. */

/* Rules for nvm request ready signal. */
rule nvm_request_ready (rg_nvm_get_data_state != IDLE && 
   rg_nvm_get_data_dword_count == rg_nvm_get_data_length - 1);
		dwr_out_nvm_request_ready <= 1'b0;
endrule: nvm_request_ready

rule nvm_request_ready1 (dwr_out_nvm_get_data_start == True);
		dwr_out_nvm_request_ready <= 1'b0;
endrule: nvm_request_ready1
/* End of rules for nvm request ready signal. */


method ActionValue#(UInt#(64)) _allocate_prp_();
	/* Allocate a Physical Region Page. */
	UInt#(TLog#(`NO_PRPs)) lv_prp_id = 0;

	// get the page address of the  next free page
	// count down to get the free PRP with the smallest number
	for (Integer i = `NO_PRPs - 1; i >= 0; i = i - 1) begin
		if (rg_prp_allocated[i] == False) begin
			lv_prp_id = fromInteger(i);
		end else if (i == 0 && lv_prp_id == 0) begin
			/* no free PRP has been found */
			// TODO better error handling
			$display("Error: All PRPs used. This is UNDEFINED!!!");
		end
	end

	// mark the page as used
	rg_prp_allocated[lv_prp_id] <= True;

	// return the PRP address consisting of base address and offset
	UInt#(TSub#(64, TAdd#(`MPS, 12))) lv_prp_base =
		'h10 + extend(lv_prp_id);  // see memory map
	UInt#(TAdd#(`MPS, 12)) offset = 0;
	let lv_prp_addr = unpack({pack(lv_prp_base), pack(offset)});
	return lv_prp_addr;
endmethod: _allocate_prp_

method Action _free_prp(UInt#(64) _prp_addr);
	/* Free the allocated PRP with the address '_prp_addr'. */
	let lv_prp_base = unpack(pack(_prp_addr)[63:`MPS + 12]);
	UInt#(64) lv_tmp = lv_prp_base - 'h10;  // see memory map
	UInt#(TLog#(`NO_PRPs)) lv_prp_id = truncate(lv_tmp);
	rg_prp_allocated[lv_prp_id] <= False;
endmethod: _free_prp

method Action _request_prp(UInt#(64) _prp_addr) if (rg_get_prp_count == 0);
	/* Request the read of PRP.

	_prp_addr: Address of the PRP
	The result has to be fetched by the get_prp_ method
	*/
	$display("%d: main_memory_model: PRP requested from 0x%x", $stime(), _prp_addr);
	rg_get_prp_addr <= _prp_addr;
	main_mem.portA.request.put(BRAMRequest {
		write: False,
		responseOnWrite: False,
		address:
			unpack(pack(_prp_addr)[`MAIN_MEM_ADDR_SIZE + valueOf(TLog#(TDiv#(32, 8))) - 1:valueOf(TLog#(TDiv#(`WDC, 8)))]),
		datain: ?
	});
	rg_get_prp_count <= rg_get_prp_count + 1;
endmethod: _request_prp

method ActionValue#(Bit#(`WDC)) get_prp_() if (rg_get_prp_count != 0);
	/* Return the PRP which has been requested by _request_prp. */
	if (rg_get_prp_count ==
		fromInteger(valueOf(TDiv#(TExp#(TAdd#(`MPS, 12)), TDiv#(`WDC, 8))))) begin
		/* all dwords of the PRP are fetched */
		rg_get_prp_count <= 0;
	end else begin
		main_mem.portA.request.put(BRAMRequest {
			write: False,
			responseOnWrite: False,
			address:
				unpack(pack(rg_get_prp_addr)[`MAIN_MEM_ADDR_SIZE + valueOf(TLog#(TDiv#(32, 8))) - 1:valueOf(TLog#(TDiv#(`WDC, 8)))]) +
				extend(rg_get_prp_count),
			datain: ?
		});
		rg_get_prp_count <= rg_get_prp_count + 1;
	end

	let lv_response <- main_mem.portA.response.get();
	$display("%d: main_memory_model: PRP data 0x%x", $stime(), lv_response);
	return lv_response;
endmethod: get_prp_

method Action _put_cmd(Nvm_command_type _cmd, UInt#(64) _sq_addr) if
	(rg_put_cmd_state == IDLE);
	/* Write a command to a submission queue.

	_sq_addr: Address of the submission queue
	After calling this method, the method put_cmd_wait_ has to be called to wait
	for the completion of the write request.
	This method fires only when the Put Command state machine is ready to run.
	*/
	$display("%d: main_memory_model: Write command with opcode %d to queue with address 0x%x",
		$stime(), _cmd.opcode, _sq_addr);
	rg_put_cmd_sq_addr <= _sq_addr;
	rg_put_cmd_command <= _cmd;
	rg_put_cmd_state <= START;
endmethod: _put_cmd

method Action _put_cmd_wait() if (rg_put_cmd_state == IDLE);
	/* Indicates a that the command is written to main memory.

	This method fires only when the state machine is done.
	*/
endmethod: _put_cmd_wait

method Action _request_compl(UInt#(64) _cq_addr) if (rg_get_compl_state == IDLE);
	/* Request the read of a completion from a completion queue.

	_cq_addr: The address of the completion queue
	The result has to be fetched by the get_compl_ method.
	This method fires only when the Get Completion state machine is ready to run.
	*/
	rg_get_compl_cq_addr <= _cq_addr;
	rg_get_compl_state <= START;
endmethod: _request_compl

method Completion_type get_compl_() if (rg_get_compl_state == IDLE);
	/* Return the completion which has been requested by _request_compl.

	This method fires only when the state machine is done.
	*/
	return unpack(pack(arr_get_compl_data)[127:0]);
endmethod: get_compl_



method Action _nvm_put_data(Bit#(`WDC) _data, UInt#(64) _address, UInt#(32) _length);
   
      UInt#(64) lv_address;
   
   if (rg_write_count == _length)
      rg_write_count <= (`WDC/32);
   else
      rg_write_count <= rg_write_count + (`WDC/32);
   
   if (rg_write_count == (`WDC/32))
      lv_address = _address;
   else
      lv_address = rg_write_address;
   
   rg_write_address <= lv_address + (`WDC/8);
   
   $display("%d: main_memory_model: NVM requests to write 0b%x to 0x%x address: 0x%x length: %d count: %d",
	    $stime(), _data, lv_address, _address, _length, rg_write_count);
   
   
   main_mem.portB.request.put(BRAMRequest {
      write: True,
      responseOnWrite: False,
      address: unpack(pack(lv_address)[`MAIN_MEM_ADDR_SIZE + valueOf(TLog#(TDiv#(32, 8))) - 1:valueOf(TLog#(TDiv#(`WDC, 8)))]),
      datain: _data
      });

endmethod: _nvm_put_data

method bit nvm_put_data_ready_();
	return 1'b1;
endmethod: nvm_put_data_ready_

method Action _nvm_request_data(UInt#(64) _address, UInt#(32) _length) if (
	rg_nvm_get_data_state == IDLE);  // TODO why 10?
	$display("%d: main_memory_model: NVM requests to read %d dwords from 0x%x",
		$stime(), _length, _address);
	rg_nvm_get_data_addr <= _address;
	// make sure that ceil is taken before cutting the LSBs
	rg_nvm_get_data_length <=
		(_length + fromInteger(valueOf(TDiv#(`WDC, 32))) - 1) >>
		fromInteger(valueOf(TLog#(TDiv#(`WDC, 32))));
	dwr_out_nvm_get_data_start <= True;
	rg_nvm_get_data_state <= START;
endmethod: _nvm_request_data

method bit nvm_request_data_ready_();
	return dwr_out_nvm_request_ready;
endmethod: nvm_request_data_ready_

method ActionValue#(Bit#(`WDC)) _nvm_get_data_();
	let lv_data <- main_mem.portB.response.get();
	$display("%d: main_memory_model: _nvm_get_data_: 0x%x", $stime(), lv_data);
	return lv_data;
endmethod: _nvm_get_data_

endmodule: mkMain_memory_model
endpackage: main_memory_model
