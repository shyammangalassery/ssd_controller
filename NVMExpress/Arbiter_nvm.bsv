//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////


// Copyright (c) 2007--2009 Bluespec, Inc.  All rights reserved.
// $Revision: 20163 $
// $Date: 2010-04-12 18:29:20 +0000 (Mon, 12 Apr 2010) $
`include "global_parameters"

package Arbiter_nvm;

import Vector::*;
import BUtils::*;
import Connectable::*;

import global_definitions::*;

////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////

interface ArbiterClient_IFCC;
	method Action request(Bit#(3) _priority, Bit#(5) _grantid);
	method Action lock();
	method Bool grant();
endinterface

interface ArbiterRequest_IFCC;
	method Bool request();
	method Bool lock();
	method Action grant();
endinterface

interface Arbiter_IFCC#(numeric type count);
	interface Vector#(count, ArbiterClient_IFCC) clients;
	method Bit#(5)grant_id;
	method Action weight(Arbitration _weight);
endinterface

typedef struct{
	Bit#(8) hpw;  // DWord 15
	Bit#(8) mpw;  // DWord 14
	Bit#(8) lpw;  // DWord 13
}Arbitration_weight deriving (Bits, Eq);



////////////////////////////////////////////////////////////////////////////////
//////
/// Priority arbiter
/////
////////////////////////////////////////////////////////////////////////////////

module mkpriorityArbiter(Arbiter_IFCC#(count));

	let pcount = valueOf(count);
		Wire#(Bit#(3)) wr_priority <- mkDWire(0);
		Wire#(Bit#(5)) wr_grant_id <- mkDWire(0);
		Wire#(Vector#(count,Bool)) grant_vector <- mkBypassWire;
		Wire#(Bit#(5)) grant_id_wire  <- mkBypassWire;
		Vector#(count, PulseWire) request_vector <- replicateM(mkPulseWire);

	for (Integer i = 0; i < 3; i = i + 1) begin
		rule every(True);

			Vector#(count, Bool) grant_vector_local = replicate(False);
			Bit#(5) grant_id_local = 0;

				case(wr_priority)
				3'b001: begin // admin priority
						grant_vector_local[wr_grant_id] = True;
						grant_id_local = wr_grant_id;
						$display("%d: ********** admin prr %d is requesting priority = %d**********",
						$stime(),wr_grant_id,wr_priority);
				end

				3'b010: begin // urgent priority
						grant_vector_local[wr_grant_id] = True;
						grant_id_local = wr_grant_id;
						$display("%d: ********** urgent prr %d is requesting** ********",
						$stime(),wr_grant_id);
//						wr_priority <= 3'b000;
				end

				3'b011: begin // high,medium,low priority*/
					grant_vector_local[wr_grant_id] = True;
					grant_id_local = wr_grant_id;
					$display("%d: ********** prr is requesting rg_grant_id= %d ********",
					$stime(),wr_grant_id);
//					rg_priority <= 3'b000;
				end
				endcase

			grant_vector <= grant_vector_local;
			grant_id_wire <= grant_id_local;
		endrule
	end




Vector#(count, ArbiterClient_IFCC) client_vector = newVector;

	for (Integer x = 0; x < 3; x = x + 1)

		client_vector[x] = (interface ArbiterClient_IFCC

			method Action request(Bit#(3) _priority, Bit#(5) _grantid);
				wr_priority <= _priority;
				wr_grant_id <= _grantid;
			endmethod

			method Action lock();
				dummyAction;
			endmethod

			method grant ();
				return grant_vector[x];
			endmethod
			endinterface);

	interface clients = client_vector;
	method grant_id = grant_id_wire;
	method Action weight(Arbitration _weight);
		dummyAction;
	endmethod

endmodule:mkpriorityArbiter





////////////////////////////////////////////////////////////////////////////////
/// weighted round robin arbiter
/// 
////////////////////////////////////////////////////////////////////////////////

module mkWeightedRR (Arbiter_IFCC#(count));

	let wcount = valueOf(count);
			Vector#(count,Bool) init_value = replicate(False);
			Reg#(Vector#(count, Bool)) priority_vector <- mkReg(init_value);
			Wire#(Vector#(count,Bool)) grant_vector <- mkBypassWire;
			Wire#(Bit#(5)) grant_id_wire  <- mkBypassWire;
			Vector#(count, PulseWire) request_vector <- replicateM(mkPulseWire);

			Reg#(Bit#(3)) rg_priority <- mkReg(0);
			Reg#(Bit#(5)) rg_grant_id <- mkRegU();
			Reg#(Arbitration) rg_weight <- mkRegU();
			Vector#(count,Reg#(Bit#(8))) rg_wei_counter <- replicateM(mkReg(unpack(0)));

for(Integer x = 0; x < 3; x = x + 1) begin

rule every(True);

	Vector#(count, Bool) grant_vector_local = replicate(False);
	Bit#(5)	grant_id_local = 0;

			case(rg_priority)
			3'b011: begin
				if(rg_wei_counter[0] <= (rg_weight.hpw)) begin //high priority
					grant_id_local = rg_grant_id;
					grant_vector_local[0] = True;
					$display("%d: ********** hpw %d is requesting *********",
					$stime(),fromInteger(x));
					rg_wei_counter[0] <= rg_wei_counter[0] + 1;
				end
				else if(rg_wei_counter[0] == rg_weight.hpw)
					rg_wei_counter[0] <= 0;
			end

			3'b010: begin
				if(rg_wei_counter[1] <= (rg_weight.mpw)) begin //medium priority
					grant_vector_local[1] = True;
					grant_id_local = rg_grant_id;
					$display("%d: ********** mpw %d is requesting** weight = %d ********",
					$stime(),rg_grant_id,rg_wei_counter[1]);
					rg_wei_counter[1] <= rg_wei_counter[1] + 1;
				end
				else if(rg_wei_counter[1] == rg_weight.mpw)
					rg_wei_counter[1] <= 0;
			end

			3'b001: begin
				if(rg_wei_counter[2] <= (rg_weight.lpw)) begin //low priority
					grant_vector_local[2] = True;
					grant_id_local = rg_grant_id;
					rg_wei_counter[2] <= rg_wei_counter[2] + 1;
				end
				else if(rg_wei_counter[2] == rg_weight.hpw)
					rg_wei_counter[2] <= 0;
			end
			endcase

	grant_vector  <= grant_vector_local;
	grant_id_wire <= grant_id_local;

	if (any(isTrue,grant_vector_local))
	begin
	$display("(%5d) Updating priorities", $time);
	priority_vector <= rotateR(grant_vector_local);
	end
endrule
end



	// Now create the vector of interfaces
	Vector#(count, ArbiterClient_IFCC) client_vector = newVector;

	for (Integer x = 0; x < 3; x = x + 1)

		client_vector[x] = (interface ArbiterClient_IFCC

			method Action request(Bit#(3) _priority, Bit#(5) _grantid);
				rg_priority <= _priority;
				rg_grant_id <= _grantid;
			endmethod

			method Action lock();
				dummyAction;
			endmethod

			method grant ();
				return grant_vector[x];
			endmethod
			endinterface);

	interface clients = client_vector;
		method grant_id = grant_id_wire;
		method Action weight(Arbitration _weight);
			rg_weight <= _weight;
		endmethod
endmodule


/*module mkHoldArbiter (Arbiter_IFCC#(count));

	let icount = valueOf(count);

	// Initially, priority is given to client 0
	Vector#(count, Bool) init_value = replicate(False);
	init_value[0] = True;
	Reg#(Vector#(count, Bool)) priority_vector <- mkReg(init_value);



	Wire#(Vector#(count, Bool)) grant_vector	<- mkBypassWire;
	Wire#(Bit#(TLog#(count)))	grant_id_wire  <- mkBypassWire;
	Vector#(count, PulseWire)	request_vector <- replicateM(mkPulseWire);

	rule every (True);

  // calculate the grant_vector
  Vector#(count, Bool) zow = replicate(False);
  Vector#(count, Bool) grant_vector_local = replicate(False);
  Bit#(TLog#(count))	grant_id_local = 0;

  Bool found = True;

  for (Integer x = 0; x < (2 * icount); x = x + 1)

	 begin

	Integer y = (x % icount);

	if (priority_vector[y]) found = False;

	let a_request = request_vector[y];
	zow[y] = a_request;

	if (!found && a_request)
		begin
		  grant_vector_local[y] = True;
		  grant_id_local= fromInteger(y);
		  found = True;
		end
	 end

  // Update the RWire
  grant_vector  <= grant_vector_local;
  grant_id_wire <= grant_id_local;

  // If a grant was given, update the priority vector so that
  // client now has lowest priority.
  if (any(isTrue,grant_vector_local))
	 begin
	priority_vector <= rotateR(grant_vector_local);
	grant_vector <= grant_vector_local;
	 end
  else
	 begin
	grant_vector <= grant_vector; // hold previous values
	 end

//  $display(" priority vector %4b", priority_vector, $time);
//  $display("  request vector %4b", zow, $time);
//  $display("Grant vector %4b", grant_vector_local, $time);

	endrule

	// Now create the vector of interfaces
	Vector#(count, ArbiterClient_IFCC) client_vector = newVector;

	for (Integer x = 0; x < icount; x = x + 1)

  client_vector[x] = (interface ArbiterClient_IFCC

			 method Action request(Bit#(3) _priority, Bit#(TLog#(count)) _grantid));
				request_vector[x].send();
			 endmethod

			 method Action lock();
				dummyAction;
			 endmethod

			 method grant ();
				return grant_vector[x];
			 endmethod
			  endinterface);

	interface clients = client_vector;
	method grant_id = grant_id_wire;
	method Action weight(Arbitration _weight);
		dummyAction;
	endmethod
endmodule*/

function Bool isTrue(Bool value);
	return value;
endfunction

instance Connectable#(ArbiterClient_IFCC,  ArbiterRequest_IFCC);
	module mkConnection#(ArbiterClient_IFCC client, ArbiterRequest_IFCC request) (Empty);

	rule send_grant (client.grant);
		request.grant();
	endrule

/*	rule send_request (request.request());
		client.request(Bit#(3) _priority);
	endrule*/

	rule send_lock (request.lock);
		client.lock();
	endrule
endmodule
endinstance

////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////

typeclass Arbitable#(type a);
	module mkArbiterRequest#(a ifc) (ArbiterRequest_IFCC);
endtypeclass

////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////

endpackage
