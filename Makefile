

TOP_MODULE:=mkTb_for_nvm_controller
TOP_FILE:=tb_nvm_controller.bsv
TOP_DIR:=./NVMExpress/BSV_src
WORKING_DIR := $(shell pwd)
VERILOGDIR:=./verilog/
NVME_SRC:=./NVMExpress/BSV_src/.

BSVINCDIR:= .:%/Prelude:%/Libraries:%/Libraries/BlueNoC:$(NVME_SRC)

check-blue:
	@if test -z "$$BLUESPECDIR"; then echo "BLUESPECDIR variable not set"; exit 1; fi; 
#	@if test -z "$$SHAKTI_HOME"; then echo "SHAKTI_HOME variable not set"; exit 1; fi;

.PHONY: check-restore
check-restore:
	@if [ "$(define_macros)" != "$(old_define_macros)" ];	then	make clean ;	fi;

.PHONY: generate_verilog 
generate_verilog: check-restore check-blue 
	@echo Compiling mkTb_for_nvm_controller in Verilog for simulations ...
	@mkdir -p BSV_src/build_bsim; 
#	@mkdir -p $(BSVBUILDDIR); 
	@mkdir -p $(VERILOGDIR); 
	@echo "old_define_macros = $(define_macros)" > old_vars
#	bsc -u -verilog -elab -vdir $(VERILOGDIR) -bdir $(BSVBUILDDIR) -info-dir $(BSVBUILDDIR) $(define_macros) -D verilog=True $(BSVCOMPILEOPTS) -verilog-filter ${BLUESPECDIR}/bin/basicinout -p $(BSVINCDIR) -g $(TOP_MODULE) $(TOP_DIR)/$(TOP_FILE) 2>&1 | tee bsv_compile.log
	bsc -u -verilog -elab -vdir verilog -bdir BSV_src/build_bsim -info-dir BSV_src/build_bsim $(define_macros) -D verilog=True -keep-fires -suppress-warnings G0020  -opt-undetermined-vals -verilog-filter ${BLUESPECDIR}/bin/basicinout -remove-empty-rules -remove-false-rules -remove-starved-rules -p $(BSVINCDIR) -g $(TOP_MODULE) $(TOP_DIR)/$(TOP_FILE)
#	@cp ./bfm/* ./verilog/.
	@echo Compilation finished

.PHONY: link_ncverilog
link_ncverilog: 
	@echo "Linking $(TOP_MODULE) using ncverilog..."
	@rm -rf work bin/work
	@mkdir -p bin 
	@mkdir work
	@ln -f -s ./verilog/cds.lib cds.lib
	@ln -f -s ./verilog/hdl.var hdl.var
	@ncvlog -sv -cdslib ./cds.lib -hdlvar ./hdl.var  +define+TOP=mkTb_for_nvm_controller ./verilog/main.v -y ./verilog/ 
	@ncelab  -cdslib ./cds.lib -hdlvar ./hdl.var -mess -NOWARN CUDEFB work.main -access +r -timescale 1ns/1ps
	@echo 'ncsim -cdslib ./cds.lib -hdlvar ./hdl.var work.main #> /dev/null' > bin/out
	@cp cds.lib hdl.var bin/
	@mv work bin/
	chmod +x bin/out
	@echo Linking finished
