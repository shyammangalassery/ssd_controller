/*
 * Generated by Bluespec Compiler, version 2015.09.beta2 (build 34689, 2015-09-07)
 * 
 * On Tue Jul 19 13:21:16 IST 2016
 * 
 */
#include "bluesim_primitives.h"
#include "module_fn_shiftleft.h"


/* Constructor */
MOD_module_fn_shiftleft::MOD_module_fn_shiftleft(tSimStateHdl simHdl,
						 char const *name,
						 Module *parent)
  : Module(simHdl, name, parent)
{
  symbol_count = 0u;
  init_symbols_0();
}


/* Symbol init fns */

void MOD_module_fn_shiftleft::init_symbols_0()
{
}


/* Rule actions */


/* Methods */

tUInt64 MOD_module_fn_shiftleft::METH_fn_shiftleft(tUInt64 ARG_fn_shiftleft__input,
						   tUInt8 ARG_fn_shiftleft__shiftamt)
{
  tUInt64 PORT_fn_shiftleft;
  PORT_fn_shiftleft = primShiftL64(64u,
				   64u,
				   (tUInt64)(ARG_fn_shiftleft__input),
				   6u,
				   (tUInt8)(ARG_fn_shiftleft__shiftamt));
  return PORT_fn_shiftleft;
}

tUInt8 MOD_module_fn_shiftleft::METH_RDY_fn_shiftleft()
{
  tUInt8 PORT_RDY_fn_shiftleft;
  tUInt8 DEF_CAN_FIRE_fn_shiftleft;
  DEF_CAN_FIRE_fn_shiftleft = (tUInt8)1u;
  PORT_RDY_fn_shiftleft = DEF_CAN_FIRE_fn_shiftleft;
  return PORT_RDY_fn_shiftleft;
}


/* Reset routines */


/* Static handles to reset routines */


/* Functions for the parent module to register its reset fns */


/* Functions to set the elaborated clock id */


/* State dumping routine */
void MOD_module_fn_shiftleft::dump_state(unsigned int indent)
{
}


/* VCD dumping routines */

unsigned int MOD_module_fn_shiftleft::dump_VCD_defs(unsigned int levels)
{
  vcd_write_scope_start(sim_hdl, inst_name);
  vcd_num = vcd_reserve_ids(sim_hdl, 0u);
  unsigned int num = vcd_num;
  for (unsigned int clk = 0u; clk < bk_num_clocks(sim_hdl); ++clk)
    vcd_add_clock_def(sim_hdl, this, bk_clock_name(sim_hdl, clk), bk_clock_vcd_num(sim_hdl, clk));
  vcd_write_scope_end(sim_hdl);
  return num;
}

void MOD_module_fn_shiftleft::dump_VCD(tVCDDumpType dt,
				       unsigned int levels,
				       MOD_module_fn_shiftleft &backing)
{
}
