/*
Copyright (c) 2016, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Module Name  : Compression and Encryption Module
Author Names : Vinod.G
Email ID : g.vinod1993@gmail.com
last updated : 30th September 2016

Status : 
    1. A dummy Compression and Encryption module is provided which simply returns the incoming TLM Packet from the PCIe. The module will be updated with sufficient compression and encryption algorithm soon.
*/
package compencr_engine.bsv;
import TLM2::*;
import TLMReqRsp::*;
import FIFOF::*;
import DReg::*;
`include "TLM.defines"
`include "defined_parameters.bsv"


interface Ifc_compencr_engine;
    interface TLMRecvIFC#(Req_tlm,Rsp_tlm) rcv_packets_to_compress;
endinterface

module mkcompencr_engine(Ifc_compencr_engine);

FIFOF#(Req_tlm) req_from_pcie <- mkSizedBypassFIFOF(1);
FIFOF#(Rsp_tlm) rsp_to_pcie <- mkSizedBypassFIFOF(1);
Reg#(Bit#(`Addr_width)) rg_addr <- mkReg(0);
Reg#(Bit#(`Reg_width)) rg_data <- mkReg(0);
Reg#(Bit#(4)) rg_id <- mkReg(0);
Reg#(Bool) rg_state <- mkDReg(False);

rule get_request_from_pcie(req_from_pcie.first matches tagged Descriptor .packet_data);
 req_from_pcie.deq;
 rg_addr <= packet_data.addr;
 rg_data <= packet_data.data;
 rg_id <= packet_data.transaction_id;
 rg_state <= True;

 //TODO Add Some compression or encryption algorithm, for now let it return some random string through rsp_to_pcie FIFO
endrule

rule send_response_to_pcie(rg_state == True);
Rsp_tlm resp_to_pcie = ?;
resp_to_pcie.status = SUCCESS;
resp_to_pcie.transaction_id = rg_id;
resp_to_pcie.data = rg_data;
resp_to_pcie.addr = rg_addr;
rsp_to_pcie.enq(resp_to_pcie);
endrule


interface TLMRecvIFC rcv_packets_to_compress = toRecvIFC (req_from_pcie,rsp_to_pcie);

endmodule
endpackage
